const express = require('express')
const puppeteer = require('puppeteer')
const yaml = require('js-yaml');
const fs = require('fs')
const app = express()
const port = 7010

let appConfig;
try {
  appConfig = yaml.safeLoad(fs.readFileSync('app.config.yaml', 'utf8'));
  console.log(appConfig);
} catch (e) {
  console.log(e);
}

let docPath = 'data/test001.pdf';

  
async function printPDF(contentUrl, headerTemplate, footerTemplate) {

  //const browser = await puppeteer.launch({ headless: true });
  console.log("appConfig.browserWSEndpoint is:" + appConfig.browserWSEndpoint);
  const browser = await puppeteer.connect({ browserWSEndpoint: appConfig.browserWsEndpoint });
  console.log("browser established.")
  const page = await browser.newPage();

  //await page.goto('https://blog.risingstack.com', {waitUntil: 'networkidle0'});
  await page.goto(contentUrl, {waitUntil: 'networkidle0'});
  const pdf = await page.pdf({
    path: docPath,
      format: 'A4',
      displayHeaderFooter: true,
      headerTemplate: headerTemplate,
      footerTemplate: footerTemplate,
      margin: {
        top: '150px',
        bottom: '80px',
        right: '30px',
        left: '30px',
      },
    });
  
    await browser.close();
    return pdf
}

// app.get('/', (req, res) => {
//   let imgPath = 'img/u352.png';
//   res.download(imgPath);
// })

app.use(express.json());
// POST method route
app.post('/', async function (req, res) {
  const base64Logo = fs.readFileSync('./img/u352.png', {encoding: 'base64'});
  const imgSrc = `data:image/png;base64, ${base64Logo}`;
  let  headerTemplate= `
  <div id="header-template" style="display:flex;font-size:10px !important; color:#808080; padding-left:30px; width:100%; border-bottom: 1px solid black;padding-bottom: 5px;">
    <div style="width:35%;"><img src="${imgSrc}" style="width:178px; height:80px;"></div>
    <div style="width:50%; font-size:20px; text-align:center; margin-top: 30px;">Industrial Relation Commission of NSW</div>
    <div style="width:15%; text-align:right;"></div>
  </div>`;
  let footerTemplate=`
  <div id="footer-template" style="display:flex; font-size:10px !important; color:#808080; padding-left:30px; width:100%; border-top: 1px solid black;padding-top: 5px;">
    <div style="width:15%; text-align:left;">
      <span>Page </span><span class="pageNumber"></span>
      <span> of </span>
      <span class="totalPages"></span>
    </div>
    <div style="width:70%;text-align:center;">Industrial Relation Commission of NSW</div>
    <div class="date" style="width:15%; text-align:righ;"></div>
  </div>
  `;
  console.log(req.body)
  await printPDF(req.body.contentUrl, headerTemplate, footerTemplate)
  res.download(docPath, 'google.pdf');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})